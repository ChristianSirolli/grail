"""Parse a Grail preferences file.

The syntax is essentially a bunch of RFC822 message headers, but blank
lines and lines that don't conform to the "key: value" format are
ignored rather than aborting the parsing.  Continuation lines
(starting with whitespace) are honored properly.  Only keys of the
form group--component are retained (illegal keys are assumed to be
comments).  Group and component names (but not values) are converted
to lowercase before they are used.  Values are stripped of leading and
trailing whitespace; continuations are represented by an embedded
newline plus a space; otherwise, internal whitespace is left
unchanged.

The only argument is an open file, which will be read until EOF.

The return value is a dictionary of dictionaries.  The outer
dictionary represents the groups; each inner dictionary represents the
components of its group.

"""

import string
import re

validpat = r"([-a-z0-9_]*)--([-a-z0-9_]*):(.*)$"
valid = re.compile(validpat, re.IGNORECASE)

debug = 0

def process(match):
    # print(match)
    if match:
        finish = match.end()
        # print(finish)
        return finish
    else:
        return 0
def parseprefs(fp):
    """Parse a Grail preferences file.  See module docstring."""
    groups = {}
    group = None                        # used for continuation line
    lineno = 0
    while 1:
        line = fp.readline()
        if not line:
            break
        lineno = lineno + 1
        if line[0] == '#':
            continue
        # print('line:',line)
        match = valid.match(line)
        # print('match:',match)
        matched = process(match)
        # print('matched at:', matched)
        if line[0] in '\t':
            # It looks line a continuation line.
            # print('if group:',group)
            if group:
                # Continue the previous line
                value = line.strip()
                if value:
                    # print('before:',group[cn])
                    if group[cn]:
                        group[cn] = group[cn] + "\n " + value
                        # print('after:',group[cn])
                    else:
                        group[cn] = value
                        # print('after:',group[cn])
        elif matched > 0:
            # It's a header line.
            groupname, cn, value = match.group(1, 2, 3) # valid.group(1, 2, 3)
            groupname = groupname.lower()
            cn = cn.lower()
            value = value.strip()
            if not groupname in groups:
                # print('before:',group)
                groups[groupname] = group = {}
                # print('after:',group)
            else:
                # print('before:',group)
                group = groups[groupname]
                # print('after:',group)
            # print('before:',group)
            group[cn] = value  # XXX Override a previous value
            # print('after:',group)
        elif line.strip() != "":
            # It's a bad line.  Ignore it.
            if debug:
                print("Error at", lineno, ":", 'line')
    # print(groups)
    return groups


def test():
    """Test program for parseprefs().

    This takes a filename as command line argument;
    if no filename is given, it parses ../data/grail-defaults.
    It also times how long it takes.

    """
    import sys
    import time
    global debug
    debug = 1
    if sys.argv[1:]:
        fn = sys.argv[1]
    else:
        fn = "../data/grail-defaults"
    fp = open(fn)
    t0 = time.time()
    groups = parseprefs(fp)
    t1 = time.time()
    fp.close()
    print("Parsing time", round(t1-t0, 3))
    groupnames = groups.keys()
    groupnames.sort()
    for groupname in groupnames:
        print("\n", groupname)
        print('=' * len(groupname), "\n")
        group = groups[groupname]
        componentnames = group.keys()
        componentnames.sort()
        for cn in componentnames:
            value = group[cn]
            print(cn + ":", repr(value))


if __name__ == '__main__':
    test()
