<!-- :
@SET "LOCATION=%CD%"
@ECHO OFF
@TITLE Think ^>^>^<^>^> Differently
@COLOR 27
@CLS
@ECHO:                                                ___________
@ECHO:                                               ^/           ^\
@ECHO:                                               ^|   THINK   ^|
@ECHO:                                               ^| ^> ^> ^< ^> ^> ^|
@ECHO:                                               ^|DIFFERENTLY^|
@ECHO:                                               ^\___________^/
@ECHO.
@ECHO.
@ECHO:Please report issues at the following URL:
@ECHO: https://gitlab.com/ChristianSirolli/Entoli/issues
@TIMEOUT /T 02>nul
@GOTO SETTINGS
:SETTINGS
@ECHO OFF
@REM Change the color of the console
@SET "COLORS=0A"
@REM Set to TRUE to have ECHO equal ON or set to FALSE to have ECHO equal OFF when started
@SET "ECHOTOGGLE=TRUE"
@REM Replace %USERPROFILE% with whatever location you would like to start with. %USERPROFILE% is equal to "C:\Users\<your username>"
@SET "CDDIR=C:\Users\christian.sirolli\OneDrive - Marion County School District\Documents\Scripts\Python\grail"
@REM Have a custom PATH variable
@SET "PATH=C:\Users\christian.sirolli\OneDrive - Marion County School District\Documents\Git\bin;C:\Users\christian.sirolli\OneDrive - Marion County School District\Documents\WPy-3670\python-3.6.7.amd64;%PATH%"
@GOTO START
:START
@ECHO OFF
@SETLOCAL EnableDelayedExpansion
@TITLE ^>^>^<^>^> Entoli
@CD %CDDIR%
@COLOR %COLORS%
@CLS
@ECHO:Entoli [Version 1.1.1.1]
@ECHO:Copyright (c) 2019 Christian Sirolli
:CMD
@ECHO OFF
@CD %CDDIR%
@IF /I "%ECHOTOGGLE%"=="true" (
	@SET "ECHO=on"
	@ECHO.
	@SET "CMD="
	@SET /P "CMD=%CDDIR%>"
)
@IF /I "%ECHOTOGGLE%"=="false" (
	@SET "ECHO=off"
	@SET "CMD="
	@SET /P CMD=""
)
@IF /I "%CMD%"=="" (GOTO CMD)
@IF /I "%CMD%"=="@ECHO OFF" (SET "ECHOTOGGLE=false" && GOTO CMD)
@IF /I "%CMD%"=="ECHO OFF" (SET "ECHOTOGGLE=false" && GOTO CMD)
@IF /I "%CMD%"=="@ECHO ON" (SET "ECHOTOGGLE=true" && GOTO CMD)
@IF /I "%CMD%"=="ECHO ON" (SET "ECHOTOGGLE=true" && GOTO CMD)
@IF /I "%CMD%"=="exit" (GOTO EXIT)
@IF /I "%CMD%"=="echo" (ECHO:ECHO is %ECHO%. && GOTO CONTINUE)
@IF /I "%CMD%"=="debug" (CD "%LOCATION%" && CALL "%LOCATION%/debug.bat" && EXIT)
@IF /I "%CMD%"=="pull" (cd "C:\Users\christian.sirolli\OneDrive - Marion County School District\Documents\Scripts\Python\grail" && git checkout master && git pull && SET "CMD=" && GOTO CMD)
@IF /I "%CMD%"=="push" (cd "C:\Users\christian.sirolli\OneDrive - Marion County School District\Documents\Scripts\Python\grail" && git add . && git commit -m "Updating files from OneDrive" && git push -u origin master && SET "CMD=" && GOTO CMD)
@IF /I "%CMD:~-3%"==".py" (CALL python.exe %CMD% && SET "CMD=" && GOTO CMD)
@echo "%CMD%"|findstr /I /R /C:"\<ECHO\ ON\ " >nul 2>&1
@if not errorlevel 1 (
	@SET "ECHOTOGGLE=true"
	CALL %CMD%
	ECHO OFF
	GOTO CONTINUE
) else (
	@echo "%CMD%"|findstr /I /R /C:"\<ECHO\ OFF\ " >nul 2>&1
	@if not errorlevel 1 (
		@SET "ECHOTOGGLE=false"
		CALL %CMD%
		ECHO OFF
		GOTO CONTINUE
	) else (
		@echo "%CMD%"|findstr /I /R /C:"\ ECHO\ ON\>" >nul 2>&1
		@if not errorlevel 1 (
			@SET "ECHOTOGGLE=true"
			CALL %CMD%
			ECHO OFF
			GOTO CONTINUE
		) else (
			@echo "%CMD%"|findstr /I /R /C:"\ ECHO\ OFF\>" >nul 2>&1
			@if not errorlevel 1 (
				@SET "ECHOTOGGLE=false"
				CALL %CMD%
				ECHO OFF
				GOTO CONTINUE
			) else (
				@echo "%CMD%"|findstr /I /R /C:"/?" >nul 2>&1
				@if not errorlevel 1 (
					%CMD%
				) else (
					@echo "%CMD%"|findstr /I /R /C:"-?" >nul 2>&1
					@if not errorlevel 1 (
						%CMD%
					) else (
						CALL %CMD%
					))))))
:CONTINUE
@SET "CDDIR=%CD%"
@GOTO CMD
:EXIT
@ENDLOCAL
@CD %LOCATION%
@ECHO OFF
@CSCRIPT //nologo "%~f0?.wsf" %*
@EXIT /B
-->
<job><script language="VBScript">
  msgbox "Thanks for using Entoli.", vbInformation+VbMsgBoxSETForeground, "Exiting Entoli - Good Bye and God Bless!"
</script></job>