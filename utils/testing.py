"""Handy dandy routines for exercising yer code.

Use exercise() to try things that shouldn't fail, or things that should
fail with a known exception, and raise an exception only if an
unanticipated exception is encountered.

Use note() to emit an error message to standard error."""
import os

__path__ = os.path.dirname(os.path.abspath(__file__))
__version__ = "$Revision: 2.8 $"

# TestFailure = 'TestFailure'
class TestFailure(Exception):
    def __init__(self, message):#, errors=message):

        # Call the base class constructor with the parameters it needs
        super().__init__('TestFailure: '+message)

        # Now for your custom code...
        self.errors = 'TestFailure: '+message#errors

def note(msg, *args):
    """Emit message to stderr, formatting message string with optional args."""
    import sys
    sys.stderr.write(msg.format(args) + '\n')

def exercise(stmt, env, purpose, expected_exception=None, verbose=0):
    """exec a statement in an environment, catching expected exceptions if any.

    exec STATEMENT in ENV.

    PURPOSE describes the statements intended effect, for failure reports.

    Optional EXPECTED_EXCEPTION indicates exception that code is *supposed*
    to raise.

    If optional VERBOSE is set, note() exec of the statement."""
    
    import sys

    try:
        if verbose: note("Exercise: exec({} in env)", repr(stmt))
        exec(stmt in env) # exec(stmt in env)
        if expected_exception:
            raise TestFailure("Unanticipated success, {0} ({1})".format(repr(stmt), purpose))
        return
    except:
        if sys.exc_info()[0] == expected_exception: #sys.exc_info()[0] == expected_exception:
            return
        else:
            raise sys.exc_info()[0](sys.exc_info()[1], sys.exc_info()[2])

ModEnv = vars()
def test_exercise(verbose=0):
    """Exercise exercise, and demonstrate usage..."""
    env = ModEnv
    exercise('testee = 1',
             env, "Innocuous assignment", None, verbose)
    exercise('if testee != 1: raise SystemError("env not working")',
             env, "Verify assignment", None, verbose)
    exercise('x(1) = 17',
             env, "Expecting basic syntax error", SyntaxError, verbose)
    exercise('{}[1]',
             env, "Expecting basic key error.", KeyError, verbose)

    env['env'] = env
    exercise("""exercise('1', env, 'Falsely expecting syntax error',
                         SyntaxError, {})""".format(verbose),
             env, "Capturing exercise exercise error", TestFailure, verbose)

    print('test_exercise() passed.')
if __name__ == "__main__":

    test_exercise()
